/**
 * User: tf
 * Date: 23.09.2014
 * Time: 13:37
 */


var ratio = 0;
var idleTime = 0;
var widthHeightRatio = 0;

function getWidthHeightRatio(){
    widthHeightRatio = $(window).width() / $(window).height();
    if(widthHeightRatio <= 1.35){
        $('body').addClass('res4to3');
    } else {
        $('body').removeClass('res4to3');
    }
}

function scaleApp() {
    var ww = $(window).width();
     var mw = 1366;
     ratio = ww / mw;
     $('#wrapper')
     .css({
     '-webkit-transform': 'scale(' + ratio + ')',
     '-moz-transform': 'scale(' + ratio + ')',
     '-ms-transform': 'scale(' + ratio + ')',
     '-o-transform': 'scale(' + ratio + ')',
     'transform': 'scale(' + ratio + ')'
     });
}

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 3) { // 15 sec
        $('#touchOverlay, #touchPulse, #touchPointer').fadeIn();
    }
}

function setIdleToZero() {
    idleTime = 0;
    $('#touchOverlay, #touchPulse, #touchPointer').hide();
}

function showGridView(){
    hideCatalog();
    hideSearch();
    $('.view').removeClass('active');
    $('.gridView').addClass('active');
}

function showDetailView(){
    hideCatalog();
    hideSearch();
    $('.homebutton').show();
    $('.view').removeClass('active');
    $('.detailView').addClass('active');

}

function showSearchView(){
    hideCatalog();
    hideSearch();
    $('.homebutton').show();
    $('.view').removeClass('active');
    $('.searchView').addClass('active');
}

function toggleCatalog(){
    hideSearch();
    $('#catalog, .catalog').toggleClass('active');
    console.log("t656265165owghxtjwdtxujwdtwdjtwdjdxctwdjxctwjxtwjxtwsj");
}

function hideCatalog(){
    $('#catalog, .catalog').removeClass('active');
}

function toggleSearch(){
    hideCatalog();
    $('#searchField, .searchButton').toggleClass('active');
    $('#searchField.active #searchInput').focus();
}

function hideSearch(){
    $('#searchField, .searchButton').removeClass('active');
    $('#searchInput').blur();
}

function setContentDimensions() {
    $('#catalog').height($('#wrapper').height() - $('.pageHeader').height()-parseInt($('#catalog').css('padding-top')) -parseInt($('#catalog').css('padding-bottom')) );
    $('#catalog, #searchField').css('top',$('.pageHeader').height());
    $('.pageContent').height($('#wrapper').height() - $('.pageHeader').height() - parseInt($('.pageContent').css('padding-top')) - parseInt($('.pageContent').css('padding-bottom')) - parseInt($('.pageContent').css('margin-top')) - parseInt($('.pageContent').css('margin-bottom')));
}

$(document).ready(function () {
                  
                  

         $('#no').click(function() {      
            $.unblockUI(); 
            return false; 
        });       
                  
                  $(".page").on('click', '.logo', function(event)
                                {
                                $(location).attr('href', 'login.html');
                                });


function indexedDBOk() 
    {
    console.log("indexedDBOk function");
      return "indexedDB" in window;
  }

if(!indexedDBOk) return;
      var openRequest = indexedDB.open("ashley",1);
      openRequest.onupgradeneeded = function(e) {
        var thisDB = e.target.result;

        if(!thisDB.objectStoreNames.contains("favourite_products")) {
                    thisDB.createObjectStore("favourite_products");
                }
        
        if(!thisDB.objectStoreNames.contains("products")) {
          thisDB.createObjectStore("products");}
        if(!thisDB.objectStoreNames.contains("category")) {
          thisDB.createObjectStore("category");        
        }
        if(!thisDB.objectStoreNames.contains("product_vs_categories")) {
          thisDB.createObjectStore("product_vs_categories");         
        }
        if(!thisDB.objectStoreNames.contains("product_vs_images")) {
          thisDB.createObjectStore("product_vs_images");         
        }
                if(!thisDB.objectStoreNames.contains("favourite_products")) {
                    thisDB.createObjectStore("favourite_products");
                }

        if(!thisDB.objectStoreNames.contains("dealer_device_info")) 
        {
                    thisDB.createObjectStore("dealer_device_info");
        }
        console.log("if(!thisDB.objectStoreNames.contains(dealer_device_info)) ");



}




openRequest.onsuccess = function(e)
{
var db;
   db = e.target.result;
   var transaction = db.transaction(["dealer_device_info"],IDBTransaction.READ_ONLY);
   var store = transaction.objectStore("dealer_device_info");
   
store.openCursor().onsuccess = function(event)
    {
        var cursor = event.target.result;
        if(cursor)
        {
            var in_marketing=cursor.value.in_marketing;
            
            
            if(in_marketing==1)
            {
              
                $(".pageHeader").append('<div class="homebutton headerButton" id="favourites" style="display:block;">My Favorites</div>');
            }

        }
}
     
     }             

     $(".page").on('click', '#favourites', function(event)
                                {
                                $(location).attr('href', 'favourites.html');
                                });
                  

                  
                  
    getWidthHeightRatio();
    setContentDimensions();
    scaleApp();
    showGridView();


$("#searchDiv").click(function(){   

  $("#searchInput").val("");
  console.log("searchDiv clicked");
});





$('#searchInput').keypress(function(event){



                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                      var searchTag = $("#searchInput").val();
                      if(searchTag == "" || searchTag == " " || searchTag == null )
                        {
                            $("#searchInput").val("");

                            navigator.notification.alert(
              "Search input cannot be blank",    // message
              null,       // callback
              "Alert", // title
              'OK'        // buttonName

          );
                            console.log("enter some text to search");
        $("#alertmsg").empty();
  $("#alertmsg").append("Enter some text to search");
  $.blockUI({ message: $('#question'), css: ({ "width": "180px","height":"80px",fontSize: "6px"}) })
                        }
                        else
                        {
                            $("#searchInput").val("");

                  localStorage.setItem("search_tag",searchTag);
                  $(location).attr("href","search.html")
                        }
                }
                event.stopPropagation();
            });


$("#search").click(function(){

   var searchTag = $("#searchInput").val();
if(searchTag == "" || searchTag == " " || searchTag == null )
{  $("#searchInput").val("");

        $("#alertmsg").empty();
  $("#alertmsg").append("Enter some text to search");
  $.blockUI({ message: $('#question'), css: ({ "width": "180px","height":"80px",fontSize: "6px"}) });
}
else
{
  $("#searchInput").val("");

  localStorage.setItem("search_tag",searchTag);

$(location).attr("href","search.html")
}

})


    $('.homebutton, .logo').on('tap', function () {
        showGridView();
    });

	
	$('.box1').on('tap', function () {
        showDetailView();
    });

    $('.catalog').on('click', function () {
        
console.log("slide toggle cat cvalllsdjsgdsdg");
  toggleCatalog();

       

    });
                  
           function checkmarketing()
                  {
                  
                  
                  
                  }
                  

    $(".page").on('click', 'a.hasSub', function(event) 
        {
           
            var parentid=$(this).closest("li").attr("id");
            var parentclass=$(this).closest("li").attr("class");
             if(parentclass=="maincatul")
             {
                var isopened=$(this).parent().find('ul').is( ".opened" );
                /*alert("is opened.."+isopened);*/
                if(isopened==false)
                {
                $('.categoryList ul ul').slideUp();
               
                }
                else if(isopened==true)
                {
                     $('.categoryList > ul > li > a').removeClass('active');
                }
             }
             /*$('.categoryList ul ul').not("a.hasSub li#"+parentid+" ul li ul")slideUp();*/
             
            $(this).addClass('active').parent().find('ul').not("li#"+parentid+" ul li ul").slideToggle().toggleClass('opened');
        });

    $('.additionalImage').on('tap', function () {
        $('#detailMainImage').css('background-image','url(../images/dummy03.jpg)');
    });

    $('.searchButton').on('click', function () {
      $("#searchInput").val("");
        toggleSearch();
       
    });

    $('.sortLink').on('tap', function () {
        $('.sortLink').removeClass('active');
        $(this).addClass('active');
    });

    $('#sortByStyle').on('tap', function () {
        $('#catalogCategories').addClass('styles');
    });

    $('#sortByRoom').on('tap', function () {
        $('#catalogCategories').removeClass('styles');
    });

    $('.categoryList a').on('tap', function () {
        $(this).closest('ul').find('a').removeClass('activeColor');
        $(this).addClass('activeColor');
    });



    $('.categoryList a').not('.hasSub').on('tap', function(){
        setTimeout(hideCatalog,200);
    });

    $('h2').on('tap', function () {
        if($(this).hasClass('active')){
            // do nothing
        } else {
            $('h2').removeClass('active').next().slideUp();
            $(this).addClass('active').next().slideDown();
        }
    }).next().hide();
    $('h2:first').addClass('active').next().show();


    var idleInterval = setInterval(timerIncrement, 5000);
    $(window).on('tap touchstart touchend touchmove click', function () {
        setIdleToZero();
    });
});

function deviceInfo()
{


var infoKey = "api_key=OXe5fWizFQhu4acLd37llxqaYoEcryS1&device_id="+localStorage.getItem("device_id");

$.ajax({
  type: "POST", 
  url: "http://ashley.catalogkiosk.com/api/v1/get_device_info",
  contentType: "application/x-www-form-urlencoded",
  data :infoKey,
  dataType: "json",                   
  async: true, 
  success:function(result)
  {
    localStorage.setItem("display_price",result.display_price);
    localStorage.setItem("status",result.status);
    localStorage.setItem("dealer_logo_url",result.dealer_logo_url);
    var logo_url=localStorage.getItem("dealer_logo_url");

 
},
    error:function(error)
  {
console.log(JSON.stringify(error))
  }
  })


}


$(window).resize(function () {
    setContentDimensions();
    scaleApp();
    getWidthHeightRatio();
});

$(window).on("orientationchange", function (event) {
    setContentDimensions();
    scaleApp();
    getWidthHeightRatio();
});

