var fav_cursor_count=0;
$(document).ready(function(){
  $('.logo').click(function(){
 $(location).attr("href","logo.html");
});

$("#list") .on("click","li",function() { 
     var id= $(this).attr("id"); 
     localStorage.setItem("prod__id",id);
    
     $(location).attr("href","ormond-beach-firm.html");
  });
 function indexedDBOk() {
     return "indexedDB" in window;
  } 
if(!indexedDBOk) return;
      var openRequest = indexedDB.open("ashley",1);
  openRequest.onsuccess = function(e) {
  db=e.target.result;
  var transaction = db.transaction(["favourite_products","products","product_vs_images"],"readonly");
  var store_fav = transaction.objectStore("favourite_products"); 
  var store_pdt = transaction.objectStore("products");
  var store_pvi=  transaction.objectStore("product_vs_images");
  var cursor_fav;
  var cursor_pdt;
  var cursor_pvi;
  store_fav.openCursor().onsuccess=function(event_fav)
  {
cursor_fav=event_fav.target.result;
if(cursor_fav)
          {
            fav_cursor_count++;
            var pid_prod=cursor_fav.value.product_id;
            store_pdt.openCursor().onsuccess=function(event_pdt)
              {
               
                cursor_pdt=event_pdt.target.result;
                if(cursor_pdt)
                {
                  var prodid=cursor_pdt.value.product_id;
                  var name=cursor_pdt.value.name;
                  if(pid_prod==prodid)
                  {
                    
                       store_pvi.openCursor().onsuccess=function(event_pvi)
                       {
                        cursor_pvi=event_pvi.target.result;
                      
                        if(cursor_pvi)
                        {
                            var pviid=cursor_pvi.value.product_id;
                        var imageurl=cursor_pvi.value.image_url;
                          if(pviid==prodid)
                          {
                          $('#list').append("<li id="+pviid+"><a href='#'><img src="+imageurl+"><SPAN STYLE='font-size: 30pt'> "+name+"</SPAN></a></li>").trigger("create").listview("refresh");
                          }
                             cursor_pvi.continue();
                        }

                        else
                        {
                            $("#loading").remove();
                             $("#sync").remove();
                        }
                  
                         
                        }
                  
                  
                }
                cursor_pdt.continue();
              }
            }
            cursor_fav.continue();
  }
  

}


}

})